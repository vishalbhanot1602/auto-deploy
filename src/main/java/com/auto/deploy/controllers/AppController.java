package com.auto.deploy.controllers;


import com.auto.deploy.response.AppBaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    @GetMapping("/test/develop")
    public AppBaseResponse testDeveloped() {
        AppBaseResponse response = new AppBaseResponse();
        response.setResponseCode(70701);
        response.setResponseMessage("Invoke develop code");
        return response;
    }


}
